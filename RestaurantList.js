import React, { Component } from 'react';
import { Platform, StyleSheet, Text, View, FlatList, TouchableHighlight, Alert, ScrollView } from 'react-native';
import { Image } from 'react-native-elements';
export default class RestaurantList extends Component {

    constructor(props) {
        super(props);
        this.state = {
            resultYT: []

        }

    }
    static navigationOptions = {
        //To hide the ActionBar/NavigationBar
        header: null,
    };

    getInformation = (index) => {
        let addedFriend = this.state.resultYT.find(function (element) { return element.id == index });
        console.log(addedFriend);
        this.props.navigation.navigate('RestaurantData', {
            value: addedFriend
        })
    }

    componentWillMount() {
        this.fetchData();
    }
    fetchData = () => {
        fetch('http://opentable.herokuapp.com/api/restaurants?city=India')
            .then((response) => response.json())
            .then((responsejson) => {
                const resultYT = responsejson.restaurants
                this.setState({
                    resultYT: resultYT
                })
                    .catch((error) => {
                        console.error(error);
                    });
            },
            )
    }
    render() {
        console.log(this.state.resultYT)
        let id = 147919
        console.log(this.state.resultYT[0])
        return (
            <View style={styles.container}>
                <View style={styles.FlatlistContainer}>
                    <FlatList
                        data={this.state.resultYT}
                        keyExtractor={(x, i) => i}
                        renderItem={({ item }) =>
                            <View style={styles.ListText}>
                                <Image
                                    source={{ uri: item.image_url }}
                                    style={{ width: 50, height: 50 }}
                                />
                                <Text onPress={() => this.getInformation(item.id)}>*{item.name}</Text>
                            </View>
                        }
                    />
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#ccffee',
    },
    welcome: {
        fontSize: 20,
        textAlign: 'center',
        margin: 10,
    },
    instructions: {
        textAlign: 'center',
        color: '#333333',
        marginBottom: 5,
    },
    ImageStyles: {
        alignSelf: 'stretch',
        height: 180,
        //marginTop: '40%',
    },
    FlatlistContainer: {
        //marginLeft: 10,
        marginTop: 10,
        //marginRight: 10,
        marginBottom: 10,
    },
    ListText: {
        //justifyContent: 'center',
        flexDirection: 'row',
        marginTop: 5,
        alignItems: 'center',
        //borderRadius: 10,
        paddingTop: 10,
        paddingBottom: 10,
        backgroundColor: '#ff3333'
    },
});
