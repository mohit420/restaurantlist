import { createAppContainer, createStackNavigator } from 'react-navigation';
import RestaurantData from './RestaurantData';
import RestaurantList from './RestaurantList';

const AppNavigator = createStackNavigator({
    Home: { screen: RestaurantList },
    RestaurantData: { screen: RestaurantData }
});

export default createAppContainer(AppNavigator);
