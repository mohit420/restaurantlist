import React, { Component } from 'react';
import { Platform, StyleSheet, Text, View, FlatList, TouchableHighlight } from 'react-native';
export default class RestaurantData extends Component {
    render() {
        let hello = this.props.navigation.state.params.value;
        return (
            <View style={styles.container}>
                <Text>
                    {hello.name} - {hello.city}
                </Text>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#F5FCFF',
    },
    welcome: {
        fontSize: 20,
        textAlign: 'center',
        margin: 10,
    },
    instructions: {
        textAlign: 'center',
        color: '#333333',
        marginBottom: 5,
    },
});
